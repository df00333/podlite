Rails.application.routes.draw do

  devise_for :users
  resources :playlists
  resources :favourites
  resources :albums
  resources :songs

  get 'home/contact'
  root 'home#index'
  get 'playlists', to: 'playlists#show'
  post 'playlist/searchsongs', to: 'playlists#search_song'
  post 'playlist/addsongs', to: 'playlists#add_song'
  post 'playlist/removesongs', to: 'playlists#remove_song'
  post 'playlist/removesongs', to: 'playlists#remove_song'
  get  'home/test', to: 'home#email_contact'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
