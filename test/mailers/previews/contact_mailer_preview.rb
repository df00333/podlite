# Preview all emails at http://localhost:3000/rails/mailers/contact_mailer
class ContactMailerPreview < ActionMailer::Preview
    def send_email
        name = params[:name]
        email = params[:email]
        phone = params[:phone]
        message =  params[:message]
        ContactMailer.send_email(name, email, phone, message)
      end
    

end
