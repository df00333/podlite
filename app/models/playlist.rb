class Playlist < ApplicationRecord
    has_and_belongs_to_many :songs
    belongs_to :user
    #validates :song_name, presence: true

end
