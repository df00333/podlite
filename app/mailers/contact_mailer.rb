class ContactMailer < ApplicationMailer
    default from: 'podlite@example.com'

    def send_email(name, email, phone, message)
      @email = email
      @name = name
      @phone = phone
      @message = message
      
      mail(to: email,
      phone: phone,
      message: message)

    end
    
end

