class PlaylistsController < ApplicationController
  before_action :set_playlist, only: %i[ show edit update destroy ]
  # If user is not authenticated don't allow them access to change playlists
  before_action :authenticate_user!, except: [:index, :show]
  before_action :correct_user, only: [:edit, :update, :destroy]


  # GET /playlists or /playlists.json
  def index
    @playlists = Playlist.all
  end

  # search for a song in the database using the search query
  def search_song
    @songs = Song.where("title like ?", params["search_term"]) if params["search_term"].present?
    @playlist = params["playlist_id"]
  end

  # Checks if you are the correct user for the playlist if not you get prompted with a message
  def correct_user
    @playlist = current_user.playlists.find_by(id: params[:id])
    redirect_to playlists_path, notice: "Not Aurthorized To Edit This Playlist" if @playlist.nil?
  end

  # add a song to a playlist
  def add_song
    playlist_id = params["playlist_id"]
    playlist_to_add = Playlist.find(playlist_id)

    song_id = params["song_id"]
    song_to_add = Song.find(song_id)
    
    playlist_to_add.songs << song_to_add

  end

  # remove a song to a playlist
  def remove_song
    playlist_id = params["playlist_id"]
    playlist= Playlist.find(playlist_id)

    song_id = params["song_id"]
    song_to_remove = Song.find(song_id)
    
    playlist.songs.delete(song_to_remove)

  end

  # GET /playlists/1 or /playlists/1.json
  def show
    playlist_id = params["id"]
    playlist_found = Playlist.find(playlist_id)
    
    @playlist_songs = playlist_found.songs

  end

  # GET /playlists/new
  def new
    #@playlist = Playlist.new
    @playlist = current_user.playlists.build
  end

  # GET /playlists/1/edit
  def edit
  end

  # POST /playlists or /playlists.json
  def create
    puts "In create method"
    #@playlist = Playlist.new(playlist_params)
    # Ties the current user to when the playlist is created
    @playlist = current_user.playlists.build(playlist_params)

    respond_to do |format|
      if @playlist.save
        format.html { redirect_to playlist_url(@playlist), notice: "Playlist was successfully created." }
        format.json { render :show, status: :created, location: @playlist }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @playlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /playlists/1 or /playlists/1.json
  def update
    respond_to do |format|
      if @playlist.update(playlist_params)
        format.html { redirect_to playlist_url(@playlist), notice: "Playlist was successfully updated." }
        format.json { render :show, status: :ok, location: @playlist }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @playlist.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /playlists/1 or /playlists/1.json
  def destroy
    @playlist.destroy

    respond_to do |format|
      format.html { redirect_to playlists_url, notice: "Playlist was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_playlist
      @playlist = Playlist.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def playlist_params
      params.require(:playlist).permit(:playlist_name, :song_id, :search, :user_id)
    end

    
end
