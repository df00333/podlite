class HomeController < ApplicationController
  def index
  end

  def contact 
  end

  def email_contact
    name = params[:name]
    email = params[:email]
    phone = params[:phone]
    message = params[:message]
    ContactMailer.send_email(name, email, phone, message).deliver_now

  end


  def about
  end
  


end
