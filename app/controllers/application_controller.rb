class ApplicationController < ActionController::Base

    def contact_email
        contact_mailer.with(name: name, email: email, phone: phone, message: message).send_email
    end
end
