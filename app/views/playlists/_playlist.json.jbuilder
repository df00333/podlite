json.extract! playlist, :id, :playlist_name, :created_at, :updated_at
json.url playlist_url(playlist, format: :json)
